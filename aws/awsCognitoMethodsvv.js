    CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
    poolData = { 
        UserPoolId : 'us-east-1_MezBNUhik', // Your user pool id here
        ClientId :   '6dlcrduo6n8v90bkddkm81cem4' // Your client id here
    };

    userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

    /*** User SignUp Method***/

    /*
        data = {
            'email':'string',
            'phone_number':'string',
            'name':'string',
            'website':'string',
            'gender':'string MALE/FEMALE',
            'username':'string',
            'password':'string alphanumeric 1 Capital 1 Special',
            'updated_at':'string'
        }

    */

    function signUp(data){

        var attributeList = [];

        var dataEmail = {
            Name : 'email',
            Value : data.email
        };

        var dataPhoneNumber = {
            Name : 'phone_number',
            Value : data.phone_number
        };

        var dataName = {
            Name : 'name',
            Value : data.name
        }; 

        var dataWebsite = {
            Name : 'website',
            Value : data.website
        };

        var dataGender = {
            Name : 'gender',
            Value : data.gender
        };
        var dataUpdated_at = {
            Name : 'updated_at',
            Value : data.updated_at
        };

        var attributeEmail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail);
        var attributePhoneNumber = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataPhoneNumber);
        var attributeName = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataName);
        var attributeWebsite = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataWebsite);
        var attributeGender = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataGender);
        var attributeUpdated_at = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataUpdated_at);

        attributeList.push(attributeEmail);
        attributeList.push(attributePhoneNumber);
        attributeList.push(attributeName);
        attributeList.push(attributeWebsite);
        attributeList.push(attributeGender);
        attributeList.push(attributeUpdated_at);

        userPool.signUp(data.username, data.password, attributeList, null, function(err, result){
            if (err) {
                console.log(err);
                return false;
            }
            cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsername());
            return true;
        });
    }

    /**Verify User**/
    /*
        data = {
            'username':'string',
            'verificationCode':'number'
        }

     */
     
    function verify(data){

        var username = data.username;
        var verificationCode = data.verificationCode;

        var userData = {
            Username : username,
            Pool : userPool
        };

        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.confirmRegistration(verificationCode, true, function(err, result) {
            if (err) {
                console.log(err);
                return false;
            }
            console.log('call result: ' + result);
            return true;
        });
    }

    /**
     * *
     * @param  {[object]} data = {'username':'string'}
     * @return {bool}      
     */
    function resendCode(data){
        var username = data.username;

        var userData = {
            Username : username,
            Pool : userPool
        };

        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.resendConfirmationCode(function(err, result) {
            if (err) {
                console.log(err);
                return false;
            }
            console.log('call result: ' + result);
        });
    }
    /**
     * *
     * @param  {object} data = {'username':'string','password':'string'}
     * @return {bool}      
     */
    function authenticate(data){

        var username = data.username;
        var password = data.password;

        var authenticationData = {
            Username : username,
            Password : password,
        };
        var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
        
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log('access token + ' + result.getAccessToken().getJwtToken());
                AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId : 'us-east-1_d4NMsbVii', // your identity pool id here
                Logins : {
                    // Change the key below according to the specific region your user pool is in.
                    'cognito-idp.us-east-1.amazonaws.com/us-east-1_d4NMsbVii' : result.getIdToken().getJwtToken()
                }
            });

            // Instantiate aws sdk service objects now that the credentials have been updated.
            // example: var s3 = new AWS.S3();
            return true;
        },

        onFailure: function(err) {
                //alert(err);
                console.log(err);
                return false;
        },

    });
    }
    /**
     * *
     * @param  {object} data ={'username':'string'}
     * @return {bool}     
     */
    function signOut(data){
        var username = data.username;
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.signOut();
    }
     /**
     * *
     * @param  {object} data ={'username':'string'}
     * @return {bool}     
     */
    function globalSignOut(data){
        var username = data.username;
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.globalSignOut(function(r,e){
            console.log(r);
            console.log(e);
        });
    }

    function getLoggenInUser() {

        var cognitoUser = userPool.getCurrentUser();

        if (cognitoUser != null) {
            cognitoUser.getSession(function(err, session) {
             if (err) {
                console.log(err);
                return false;
            }
          console.log('session validity: ' + session.isValid());

          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId : 'us-east-1_d4NMsbVii', // your identity pool id here
                Logins : {
                    // Change the key below according to the specific region your user pool is in.
                    'cognito-idp.us-east-1.amazonaws.com/us-east-1_d4NMsbVii' : session.getIdToken().getJwtToken()
                }
            });

            // Instantiate aws sdk service objects now that the credentials have been updated.
            // example: var s3 = new AWS.S3();

        });
        }

    }
     /**
     * *
     * @param  {object} data ={'username':'string'}
     * @return {bool}     
     */
    function getUserData() {
        var username = data.username;
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.getUserAttributes(function(err, result) {
            if (err) {
                console.log(err);
                return false;
            }
            for (i = 0; i < result.length; i++) {
                console.log('attribute ' + result[i].getName() + ' has value ' + result[i].getValue());
            }
        });
    }