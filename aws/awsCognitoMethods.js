    CognitoUserPool = AmazonCognitoIdentity.CognitoUserPool;
    poolData = { 
        UserPoolId : 'us-east-1_NKvJfYkFH', // Your user pool id here
        ClientId   : 'tdjus5nm45i4ch2n85d3f2h0v' // Your client id here
    };

    userPool = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserPool(poolData);

    /*** User SignUp Method***/

    /*
        data = {
            'email':'string',
            'phone_number':'string',
            'name':'string',
            'website':'string',
            'gender':'string MALE/FEMALE',
            'username':'string',
            'password':'string alphanumeric 1 Capital 1 Special',
            'updated_at':'string'
        }

    */

    function signUp(data){

       /* var email = $('#email').val();
        var phone_number = $('#phone_number').val();
        var username = $('#username').val();
        var password = $('#password').val();
        var name = $('#name').val();
        var website = $('#website').val();
        var gender = $('#gender').val();
        var updated_at = $('#updated_at').val();
*/

        var email = data.email;
        var phone_number = data.phone_number;
        var username = data.username;
        var password = data.password;
        var name = data.name;
        var website = data.website;
        var updated_at = data.updated_at;
         

       /* alert(email);
        alert(username);
        alert(password);*/
        //alert(phone_number+name);

        var attributeList = [];

        var dataEmail = {
            Name : 'email',
            Value : email
        };

        var dataPhoneNumber = {
            Name : 'phone_number',
            Value : phone_number
        };

        var dataName = {
            Name : 'name',
            Value : name
        }; 

        var dataWebsite = {
            Name : 'website',
            Value : website
        };

        var dataGender = {
            Name : 'gender',
            Value : gender
        };
        var dataUpdated_at = {
            Name : 'updated_at',
            Value : updated_at
        };

        var attributeEmail = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataEmail);
        var attributePhoneNumber = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataPhoneNumber);
        var attributeName = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataName);
        var attributeWebsite = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataWebsite);
        var attributeGender = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataGender);
        var attributeUpdated_at = new AWSCognito.CognitoIdentityServiceProvider.CognitoUserAttribute(dataUpdated_at);

        attributeList.push(attributeEmail);
        attributeList.push(attributePhoneNumber);
        attributeList.push(attributeName);
        attributeList.push(attributeWebsite);
        attributeList.push(attributeGender);
        attributeList.push(attributeUpdated_at);

        userPool.signUp(username, password, attributeList, null, function(err, result){
            if (err) {
                $('#response').html(err);
               // alert(err);
                return false;
            }
            cognitoUser = result.user;
            console.log('user name is ' + cognitoUser.getUsername());
            $('#response').html('user name is ' + cognitoUser.getUsername());
            //alert("Success");
            return true;
        });
    }

    function verify(){

        var username = $('#v_username').val();
        var verificationCode = $('#verificationCode').val();

        var userData = {
            Username : username,
            Pool : userPool
        };

        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.confirmRegistration(verificationCode, true, function(err, result) {
            if (err) {
                $('#response').html(err);
                return;
            }
            $('#response').html('call result: ' + result);
            console.log('call result: ' + result);
        });
    }

    function resendCode(){
        var username = $('#r_username').val();

        var userData = {
            Username : username,
            Pool : userPool
        };

        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.resendConfirmationCode(function(err, result) {
            if (err) {
                $('#response').html(err);
                return;
            }
            console.log('call result: ' + result);
            $('#response').html('call result: ' + result);
        });
    }

    function authenticate(){

        var username = $('#a_username').val();
        var password = $('#a_password').val();

        var authenticationData = {
            Username : username,
            Password : password,
        };
        var authenticationDetails = new AWSCognito.CognitoIdentityServiceProvider.AuthenticationDetails(authenticationData);
        
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.authenticateUser(authenticationDetails, {
            onSuccess: function (result) {
                console.log('access token + ' + result.getAccessToken().getJwtToken());
                $('#response').html('access token + ' + result.getAccessToken().getJwtToken());
                AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId : 'us-east-1:d4e3516b-44ea-4bc7-8017-e363343fc72a', // your identity pool id here
                Logins : {
                    // Change the key below according to the specific region your user pool is in.
                    'graph.facebook.com': '593022360907813',
                    'www.amazon.com': 'amzn1.application.c083fccf89084ddcbe0f5489d6c958d3',
                    'accounts.google.com': 'tdjus5nm45i4ch2n85d3f2h0v'
//                    'cognito-idp.us-east-1.amazonaws.com/us-east-1_d4NMsbVii' : result.getIdToken().getJwtToken()
                }
            });
            
            AWSIdentity();
            // Instantiate aws sdk service objects now that the credentials have been updated.
            // example: var s3 = new AWS.S3();

        },

        onFailure: function(err) {
            alert(err);
            $('#response').html(err);
        },

    });
    }

    function signOut(){
        var username = $('#s_username').val();
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.signOut();
    }

    function globalSignOut(){
        var username = $('#s_username').val();
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.globalSignOut(function(r,e){
            console.log(r);
            console.log(e);
        });
    }

    function getLoggenInUser() {
        console.log('getLoggenInUser');
        var cognitoUser = userPool.getCurrentUser();

        if (cognitoUser != null) {
            cognitoUser.getSession(function(err, session) {
                if (err) {
               alert(err);
              $('#response').html(err);
              return;
          }
          console.log('session validity: ' + session.isValid());

          AWS.config.credentials = new AWS.CognitoIdentityCredentials({
                IdentityPoolId : 'us-east-1_d4NMsbVii', // your identity pool id here
                Logins : {
                    // Change the key below according to the specific region your user pool is in.
                    'cognito-idp.us-east-1.amazonaws.com/us-east-1_d4NMsbVii' : session.getIdToken().getJwtToken()
                }
            });

            // Instantiate aws sdk service objects now that the credentials have been updated.
            // example: var s3 = new AWS.S3();

        });
        }

    }

    function getUserData() {
        var username = $('#g_username').val();
        console.log(username);
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
        cognitoUser.getUserAttributes(function(err, result) {
            if (err) {
                $('#response').html(err);
                return;
            }
            for (i = 0; i < result.length; i++) {
                console.log('attribute ' + result[i].getName() + ' has value ' + result[i].getValue());
            }
        });
    }
function AWSIdentity(){

     var username = $('#a_username').val();
        var password = $('#a_password').val();
    // Initialize the Amazon Cognito credentials provider
AWS.config.region = 'us-east-1'; // Region
AWS.config.credentials = new AWS.CognitoIdentityCredentials({
    IdentityPoolId: 'us-east-1:d4e3516b-44ea-4bc7-8017-e363343fc72a',
});

AWS.config.credentials.get(function(){

   var syncClient = new AWS.CognitoSyncManager();

   syncClient.openOrCreateDataset('myDataset', function(err, dataset) {

      dataset.put('username', 'username', function(err, record){

         dataset.synchronize({

            onSuccess: function(data, newRecords) {
               console.log(data);
               console.log(newRecords);
            },
            onFailure: function(err) {
                alert(err);
                $('#response').html(err);
            }

         });

      });

   });

});

}




function ForgotPasswordFunction(){

alert("calling");
   

var username ='ravik';      
        var userData = {
            Username : username,
            Pool : userPool
        };
        var cognitoUser = new AWSCognito.CognitoIdentityServiceProvider.CognitoUser(userData);
   
   cognitoUser.forgotPassword({
        onSuccess: function (result) {
            console.log('call result: ' + result);
        },
        onFailure: function(err) {
            alert(err);
        },
        inputVerificationCode() {
            var verificationCode = prompt('Please input verification code ' ,'');
            var newPassword = prompt('Enter new password ' ,'');
            cognitoUser.confirmPassword(verificationCode, newPassword, this);
        }
    });


}
